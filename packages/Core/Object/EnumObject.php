<?php

namespace Core\Object;

use Core\Validation\BaseValidation;
use UnexpectedValueException;

/**
 * Класс перечисляемых значений
 * Class EnumObject
 * @package Core\Object
 */
abstract class EnumObject
{
    use BaseValidation;

    /**
     * Допустимые значения
     * @var null
     */
    protected static $availableValues = NULL;

    /**
     * Текущее значение
     * @var string
     */
    protected $value;

    /**
     * Получить список допустимых значений
     * @return array
     */
    public static function getAvailableValues(): array
    {
        return static::$availableValues;
    }

    /**
     * Проверить значение
     * @param $value
     * @return bool
     */
    public static function isValidValue($value): bool
    {
        return in_array($value, static::$availableValues, true);
    }

    /**
     * @param string $initialValue
     * @throws UnexpectedValueException
     */
    public function __construct($initialValue)
    {
        $this->validateNonEmptyString($initialValue);
        if (!self::isValidValue($initialValue)) {
            throw new \UnexpectedValueException('Неподдерживаемое значение');
        }
        $this->value = $initialValue;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->value;
    }
}