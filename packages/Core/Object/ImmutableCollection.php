<?php

namespace Core\Object;

use Core\Validation\BaseValidation;

abstract class ImmutableCollection implements \Iterator
{
    use BaseValidation;

    /**
     * Название класса, объекты которого будет содержать коллекция
     * @var string
     */
    protected static $collectionClass = IIdObject::class;

    /**
     * Объекты
     * @var array
     */
    protected $objects;

    /**
     * ImmutableCollection constructor.
     * @param IIdObject[] $objects
     */
    public function __construct(array $objects)
    {
        $this->validateNonEmptyArray($objects);
        foreach ($objects as $object) {
            if (!($object instanceof static::$collectionClass)) {
                throw new \InvalidArgumentException('Invalid type of argument');
            }
            $this->objects[$object->getId()] = $object;
        }
    }

    /**
     * Колллекция не содержит объектов
     * @return bool
     */
    public function isEmpty(): bool
    {
        return empty($this->objects);
    }

    /**
     * Получить объект по идентификатору
     * @param $id
     * @return IIdObject|null
     */
    public function get($id): IIdObject
    {
        return $this->objects[$id];
    }

    /**
     * @return IIdObject
     */
    public function current(): IIdObject
    {
        return current($this->objects);
    }

    /**
     * @return self
     */
    public function next(): self
    {
        next($this->objects);
        return $this;
    }

    /**
     * @return int|mixed|string|null
     */
    public function key()
    {
        return key($this->objects);
    }

    /**
     * @return bool
     */
    public function valid(): bool
    {
        return null !== key($this->objects);
    }

    /**
     * @return self
     */
    public function rewind(): self
    {
        reset($this->objects);
        return $this;
    }
}