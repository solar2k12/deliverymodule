<?php

namespace Core\Validation;

use Core\Validation\Exception\InvalidDateTimeStringException;

/**
 * Трейт для валидации даты и времени
 * Trait DateTimeValidation
 * @package Core\Validation
 */
trait DateTimeValidation
{
    /**
     * Валидация строки, которая содержит дату и время
     * @param string $dateTime
     * @param string $dateFormat
     */
    protected function validateDateTimeFromString($dateTime, $dateFormat = DATE_ATOM): void
    {
        $dtObject = \DateTime::createFromFormat($dateFormat, $dateTime);
        if (!$dtObject || $dtObject->format($dateFormat) !== $dateTime) {
            throw new InvalidDateTimeStringException(
                sprintf('Некорректный формат даты и времени: "%s"', $dateTime)
            );
        }
    }
}