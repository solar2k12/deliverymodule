<?php

namespace Core\Validation\Exception;

/**
 * Класс исключения при некорректном формате даты и времени в строке
 * Class InvalidDateTimeStringException
 * @package Core\Validation\Exception
 */
class InvalidDateTimeStringException extends \InvalidArgumentException
{
}