<?php

namespace Delivery\ValueObject;

use Core\Object\EnumObject;

/**
 * Единицы измерения веса
 * Class WeightUnit
 * @package Delivery\ValueObject
 */
class WeightUnit extends  EnumObject
{
    public const KILOGRAM = 'kg';
    public const GRAM = 'g';
    public const POUND = 'lb';
    public const OUNCE = 'oz';

    protected static $availableValues = [
        self::KILOGRAM,
        self::GRAM,
        self::POUND,
        self::OUNCE
    ];
}