<?php

namespace Delivery\ValueObject;

use Core\Validation\BaseValidation;
use Delivery\Address\Exception\InvalidAddressException;
use \InvalidArgumentException;

/**
 * Класс для описания заказа на доставку
 * Class DeliveryRequest
 * @package Delivery\ValueObject
 */
class DeliveryRequest
{
    use BaseValidation;

    /**
     * Адрес отправителя
     * @var string
     */
    private $sendAddress;

    /**
     * Адрес получателя
     * @var string
     */
    private $receiveAddress;

    /**
     * Размеры посылок
     * @var PackageMeasures[]
     */
    private $packages;

    /**
     * DeliveryRequest constructor.
     * @param $sendAddress
     * @param $receiveAddress
     * @param array $packages
     */
    public function __construct($sendAddress, $receiveAddress, array $packages)
    {
        $this->validateNonEmptyString($sendAddress);
        $this->validateNonEmptyString($receiveAddress);
        if ($sendAddress === $receiveAddress) {
            throw new InvalidAddressException('Адреса получателя и отправителя должны быть разными');
        }
        $this->sendAddress = $sendAddress;
        $this->receiveAddress = $receiveAddress;
        $this->validateNonEmptyArray($packages);
        foreach ($packages as $package) {
            if (!($package instanceof PackageMeasures)) {
                throw new InvalidArgumentException('Список посылок содержит неверный тип');
            }
        }
        $this->packages = $packages;
    }

    /**
     * @return string
     */
    public function getSendAddress(): string
    {
        return $this->sendAddress;
    }

    /**
     * @return string
     */
    public function getReceiveAddress(): string
    {
        return $this->receiveAddress;
    }

    /**
     * @return PackageMeasures[]
     */
    public function getPackages(): array
    {
        return $this->packages;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $output = sprintf(
            "\nАдрес отправителя: %s\nАдрес получателя: %s\nКоличество посылок:%d",
            $this->sendAddress,
            $this->receiveAddress,
            count($this->packages)
        );
        foreach ($this->packages as $index => $package) {
            $output.= sprintf(
                "\n\tПосылка %d%s",
                $index + 1,
                $package
            );
        }
        return $output;
    }
}