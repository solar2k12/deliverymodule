<?php

namespace Delivery\ValueObject;

use Core\Validation\BaseValidation;

/**
 * Класс для представления размеров и веса посылки
 * Class PackageMeasure
 * @package Delivery\ValueObject
 */
class PackageMeasures
{
    use BaseValidation;
    
    /**
     * Вес посылки
     * @var float
     */
    private $weight;

    /**
     * Высота посылки
     * @var float
     */
    private $height;

    /**
     * Ширина посылки
     * @var float
     */
    private $width;

    /**
     * Глубина посылки
     * @var float
     */
    private $depth;

    /**
     * Единица измерения габаритов
     * @var MeasureUnit
     */
    private $hwdUnit;

    /**
     * Единица измерения веса
     * @var WeightUnit
     */
    private $weightUnit;

    /**
     * PackageMeasure constructor.
     * @param mixed $weight - вес посылки
     * @param mixed $height - высота посылки
     * @param mixed $width - ширина посылки
     * @param mixed $depth - глубина посылки
     * @param string $hwdUnit - единица измерения габаритов
     * @param string $weightUnit - единица измерения веса
     */
    public function __construct(
        $weight,
        $height,
        $width,
        $depth,
        $hwdUnit = MeasureUnit::CENTIMETER,
        $weightUnit = WeightUnit::KILOGRAM
    ) {
        $this->validatePositiveNumericValue($weight);
        $this->validatePositiveNumericValue($height);
        $this->validatePositiveNumericValue($width);
        $this->validatePositiveNumericValue($depth);
        $this->weight = (float)$weight;
        $this->height = (float)$height;
        $this->width = (float)$width;
        $this->depth = (float)$depth;
        $this->hwdUnit = new MeasureUnit($hwdUnit);
        $this->weightUnit = new WeightUnit($weightUnit);
    }

    /**
     * Возвращает объем посылки
     * @return float
     */
    public function calculateVolume(): float
    {
        return $this->height * $this->width * $this->depth;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @return float
     */
    public function getHeight(): float
    {
        return $this->height;
    }

    /**
     * @return float
     */
    public function getWidth(): float
    {
        return $this->width;
    }

    /**
     * @return float
     */
    public function getDepth(): float
    {
        return $this->depth;
    }

    /**
     * @return MeasureUnit
     */
    public function getHwdUnit(): MeasureUnit
    {
        return $this->hwdUnit;
    }

    /**
     * @return WeightUnit
     */
    public function getWeightUnit(): WeightUnit
    {
        return $this->weightUnit;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf(
            "\n\t\tГабариты:%.3f x %.3f x %.3f  %s\n\t\tВес: %.3f %s",
            $this->height,
            $this->width,
            $this->depth,
            $this->hwdUnit,
            $this->weight,
            $this->weightUnit
        );
    }
}