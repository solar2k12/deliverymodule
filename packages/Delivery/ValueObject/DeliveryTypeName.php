<?php

namespace Delivery\ValueObject;

use Core\Object\EnumObject;

/**
 * Класс описывает названия сервиса доставки
 * Class DeliveryTypeName
 * @package Delivery\ValueObject
 */
class DeliveryTypeName extends EnumObject
{
    //Название сервиса "Птичка"
    public const BIRD_DELIVERY = 'Bird';

    //Название сервиса "Черепашка"
    public const TURTLE_DELIVERY = 'Turtle';

    /**
     * @var array
     */
    protected static $availableValues = [
        self::BIRD_DELIVERY,
        self::TURTLE_DELIVERY
    ];

    /**
     * @return bool
     */
    public function isBird(): bool
    {
        return $this->value === self::BIRD_DELIVERY;
    }

    /**
     * @return bool
     */
    public function isTurtle(): bool
    {
        return $this->value === self::TURTLE_DELIVERY;
    }

    /**
     * @return DeliveryTypeName
     */
    public static function getBirdType(): DeliveryTypeName
    {
        return new self(self::BIRD_DELIVERY);
    }

    /**
     * @return DeliveryTypeName
     */
    public static function getTurtleType(): DeliveryTypeName
    {
        return new self(self::TURTLE_DELIVERY);
    }
}