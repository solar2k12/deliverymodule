<?php

namespace Delivery\ValueObject;

use Core\Object\EnumObject;

/**
 * Единицы измерения габаритов
 * Class MeasureUnit
 * @package Delivery\ValueObject
 */
class MeasureUnit extends EnumObject
{
    public const MILLIMETER = 'mm';
    public const CENTIMETER = 'cm';
    public const METER = 'm';
    public const INCH = 'inch';
    public const FOOT = 'ft';

    protected static $availableValues = [
        self::MILLIMETER,
        self::CENTIMETER,
        self::METER,
        self::FOOT,
        self::INCH
    ];
}