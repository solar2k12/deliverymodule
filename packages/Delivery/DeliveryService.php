<?php


namespace Delivery;

use Delivery\Address\IAddressValidationService;
use Delivery\DeliveryType\Result\DeliveryResultCollection;
use Delivery\ValueObject\DeliveryRequest;

/**
 * Сервис вычисления стоимости и даты доставки с помощью различных служб доставки
 * Class MainDeliveryService
 * @package Delivery
 */
class DeliveryService
{
    /**
     * Сервис валидации адреса
     * @var IAddressValidationService
     */
    private $addressValidationService;

    /**
     * Список доступных служб доставки
     * @var DeliveryTypeCollection
     */
    private $deliveryTypeCollection;

    /**
     * DeliveryService constructor.
     * @param IAddressValidationService $addressValidationService
     * @param DeliveryTypeCollection $deliveryTypeCollection
     */
    public function __construct(
        IAddressValidationService $addressValidationService,
        DeliveryTypeCollection $deliveryTypeCollection
    ) {
        $this->addressValidationService = $addressValidationService;
        if ($deliveryTypeCollection->isEmpty()) {
            throw new \InvalidArgumentException('Список служб доставки пуст');
        }
        $this->deliveryTypeCollection = $deliveryTypeCollection;
    }

    /**
     * Расчитать условия доставки
     * @param DeliveryRequest $request - запрос на доставку
     * @param array $deliveryServiceIds - идентификаторы служб доставки (DeliveryTypeName)
     * @param boolean $addDescription - добавить в результат описание
     * @return DeliveryResultCollection
     */
    public function getDeliveryConditions(
        DeliveryRequest $request,
        array $deliveryServiceIds = [],
        $addDescription = false
    ): DeliveryResultCollection
    {
        $this->addressValidationService->validateAddress($request->getSendAddress());
        $this->addressValidationService->validateAddress($request->getReceiveAddress());
        $results = [];
        foreach ($deliveryServiceIds as $serviceId) {
            if (!$this->deliveryTypeCollection->get($serviceId)) {
                throw new \RuntimeException(
                    sprintf('Служба доставки с идентификатором "%d" не поддерживается', $serviceId)
                );
            }
        }
        foreach ($this->deliveryTypeCollection as $deliveryType) {
            if (
                empty($deliveryServiceIds)
                ||
                (!empty($deliveryServiceIds) && in_array($deliveryType->getId(), $deliveryServiceIds, true))
            ) {
                $result = $deliveryType->getDeliveryCalculationResult($request, $addDescription);
                if (!$result) {
                    throw new \RuntimeException('Служба не смогла обработать запрос');
                }
                $results[] = $result;
            }
        }

        return new DeliveryResultCollection($results);
    }
}