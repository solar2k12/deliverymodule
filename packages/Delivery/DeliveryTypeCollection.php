<?php

namespace Delivery;

use Core\Object\ImmutableCollection;
use Delivery\DeliveryType\IDeliveryType;

/**
 * Class DeliveryTypeCollection
 * @package Delivery
 * @method IDeliveryType get($id)
 * @method IDeliveryType current()
 * @method DeliveryTypeCollection next()
 * @method DeliveryTypeCollection rewind()
 */
class DeliveryTypeCollection extends ImmutableCollection
{
    protected static $collectionClass = IDeliveryType::class;
}