<?php

namespace Delivery\Test;

use Delivery\ValueObject\DeliveryRequest;
use Delivery\ValueObject\DeliveryTypeName;
use Delivery\ValueObject\MeasureUnit;
use Delivery\ValueObject\PackageMeasures;
use Delivery\ValueObject\WeightUnit;

/**
 * Хранилище и генератор данных для "заглушек" BirdDeliveryCalculationServiceMock и TurtleDeliveryCalculationServiceMock
 * Class CalculationTestCases
 * @package Delivery\Test
 */
class CalculationTestCases
{
    //Минимальное количество запросов
    private const MIN_REQUESTS = 5;

    //Максимальное количество запросов
    private const MAX_REQUESTS = 20;

    //Максимальное количество посылок
    private const MAX_PACKAGES = 10;

    //Максимальный срок выполнения доставки
    private const MAX_DAYS = 30;

    /**
     * @var array
     */
    private static $requestStore = [];

    /**
     * @var array
     */
    private static $responseStore = [];

    /**
     * @param DeliveryTypeName $typeName
     * @param DeliveryRequest $request
     * @return array | null
     */
    public static function getResponse(DeliveryTypeName $typeName, DeliveryRequest $request): array
    {
        if (!isset(self::$responseStore[$typeName->getValue()][self::calculateRequestHash($request)])) {
            return null;
        }

        return self::$responseStore[$typeName->getValue()][self::calculateRequestHash($request)];
    }

    /**
     * @param DeliveryTypeName $generateResponseType
     * @param bool $validResponse - генерировать валидный ответ
     * @throws \Exception
     */
    public static function generateCases(DeliveryTypeName $generateResponseType = null, $validResponse = true): void
    {
        AddressValidationTestCases::generateAddressBook();
        $types = [$generateResponseType];
        if (!$generateResponseType) {
            $types = DeliveryTypeName::getAvailableValues();
        }
        self::$requestStore = [];

        $requestsCount = random_int(self::MIN_REQUESTS, self::MAX_REQUESTS);

        for ($i =0; $i < $requestsCount; $i++) {
            $request = self::generateRequest();
            self::$requestStore[] = $request;
            foreach ($types as $type) {
                $methodName = sprintf('generate%sResponse', $type);
                if (!in_array($methodName, get_class_methods(__CLASS__), true)) {
                    throw new \RuntimeException('Delivery type not supported');
                }
                if (!$validResponse) {
                    $methodName = 'generateInvalidResponse';
                }

                $method = [__CLASS__, $methodName];
                self::$responseStore[$type][self::calculateRequestHash($request)] = $method($generateResponseType);
            }
        }
    }

    /**
     * @return array
     */
    public static function getRequests(): array
    {
        return self::$requestStore ?? [];
    }

    /**
     * @param DeliveryTypeName $typeName
     * @return array
     */
    public static function getResponses(DeliveryTypeName $typeName): array
    {
       return self::$responseStore[$typeName->getValue()] ?? [];
    }

    /**
     * Рассчитывает хэш для запроса доставки
     * @param DeliveryRequest $request
     * @return string
     */
    public static function calculateRequestHash(DeliveryRequest $request): string
    {
        $packageHash = array_reduce(
            $request->getPackages(),
            static function ($result, $package) {
                /** @var PackageMeasures $package */
                $result.= $package->calculateVolume() . $package->getWeightUnit() . $package->getHwdUnit();
                return $result;
            }
        );

        return md5($request->getReceiveAddress() . $request->getSendAddress().$packageHash);
    }

    /**
     * @return PackageMeasures
     * @throws \Exception
     */
    public static function generateRandomPackage(): PackageMeasures
    {
        $hwdUnit = self::getRandomItem(MeasureUnit::getAvailableValues());
        $weightUnit = self::getRandomItem(WeightUnit::getAvailableValues());

        return new PackageMeasures(
            self::getRandomFloat(), 
            self::getRandomFloat(), 
            self::getRandomFloat(), 
            self::getRandomFloat(), 
            $hwdUnit, 
            $weightUnit
        );
    }

    /**
     * @return DeliveryRequest
     * @throws \Exception
     */
    public static function generateRequest():DeliveryRequest
    {
        $sendAddress = '';
        $receiveAddress = '';
        $addresses = AddressValidationTestCases::getAddresses();
        do {
            $sendAddress = self::getRandomItem($addresses);
            $receiveAddress = self::getRandomItem($addresses);
        } while ($sendAddress === $receiveAddress);
        $packages = [];
        $packagesCount = random_int(1, self::MAX_PACKAGES);

        for ($i = 0; $i < $packagesCount; $i++) {
            $packages[] = self::generateRandomPackage();
        }

        return new DeliveryRequest($sendAddress, $receiveAddress, $packages);
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function generateBirdResponse():array
    {
        return ['dateInterval' => random_int(1, self::MAX_DAYS), 'cost' => self::getRandomFloat()];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function generateTurtleResponse(): array
    {
        $addBaseCost = random_int(0, 100);
        if ($addBaseCost > 50) {
            return [
                'date' => self::getRandomDate()->format(DATE_ATOM),
                'costCoefficient' => self::getRandomFloat(),
                'baseCost' => self::getRandomFloat(0, 1000)
            ];
        }

        return [
            'date' => self::getRandomDate()->format(DATE_ATOM),
            'costCoefficient' => self::getRandomFloat(),
        ];
    }

    /**
     * @return array
     * @throws \Exception
     */
    public static function generateInvalidResponse():array
    {
        return [
            'dateInterval' => -1 * random_int(1, self::MAX_DAYS),
            'cost' => uniqid(microtime(true), true),
            'date' => uniqid(microtime(true), true),
            'costCoefficient' => -1 * self::getRandomFloat()
        ];
    }

    /**
     * Потому что array_rand выбирает почти не случайные элементы
     * @param $arrayValue
     * @return mixed
     * @throws \Exception
     */
    public static function getRandomItem($arrayValue)
    {
        return $arrayValue[random_int(0, count($arrayValue) - 1)];
    }

    /**
     * @param int $min
     * @param int $max
     * @return float
     * @throws \Exception
     */
    private static function getRandomFloat($min = 1, $max = 100): float
    {
        return random_int($min, $max - 1) + random_int(1, 99) / 100;
    }

    /**
     * @return \DateTime
     * @throws \Exception
     */
    private static function getRandomDate(): \DateTime
    {
        return (new \DateTime())->add(new \DateInterval(sprintf('P%dD', random_int(1, self::MAX_DAYS))));
    }
}