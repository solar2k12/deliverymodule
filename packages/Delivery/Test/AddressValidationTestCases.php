<?php

namespace Delivery\Test;

/**
 * Хранилище тестовых данных для класса "заглушки" валидации адресов
 * Class MockTestCases
 * @package Delivery\Test
 */
class AddressValidationTestCases
{

    private const MAX_ADDRESS_ENTRIES = 100;

    private const MIN_ADDRESS_ENTRIES = 10;

    /**
     * @var array
     */
    private static $addressStore = [];

    /**
     * @param int $min
     * @param int $max
     * @throws \Exception
     */
    public static function generateAddressBook(
        int $min = self::MIN_ADDRESS_ENTRIES,
        int $max = self::MAX_ADDRESS_ENTRIES
    ): void
    {
        self::$addressStore = [];

        if ($max < $min) {
            $min -= $max;
            $max += $min;
            $min = $max - $min;
        }
        $size = random_int((int)$min, (int)$max);

        for ($i = 0; $i <= $size; $i++) {
            self::$addressStore[] = uniqid((string)microtime(true), true);
        }
    }

    /**
     * @return array
     */
    public static function getAddresses(): array
    {
        return self::$addressStore;
    }
}