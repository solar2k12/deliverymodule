<?php

namespace Delivery\Test;

use PHPUnit\Framework\TestCase;
use Delivery\ValueObject\PackageMeasures;

/**
 * Тест на класс PackageMeasures
 * Class PackageMeasuresTest
 * @package Delivery\Test
 */
class PackageMeasuresTest extends TestCase
{
    /**
     * @dataProvider testCases
     * @param float | string | int $weight
     * @param float | string | int $height
     * @param float | string | int $width
     * @param float | string | int $depth
     * @param string $hwdUnit
     * @param string $weightUnit
     * @param float $expectedVolume
     * @param string | null $exception
     */
    public function testVolumeAndGetters(
        $weight,
        $height,
        $width,
        $depth,
        $hwdUnit,
        $weightUnit,
        $expectedVolume,
        $exception = null
    ): void
    {
        if ($exception) {
            $this->expectException($exception);
        }

        $package = new PackageMeasures($weight, $height, $width, $depth, $hwdUnit, $weightUnit);
        $this->assertEquals($expectedVolume, $package->calculateVolume());
        $this->assertEquals($weight, $package->getWeight());
        $this->assertEquals($height, $package->getHeight());
        $this->assertEquals($width, $package->getWidth());
        $this->assertEquals($depth, $package->getDepth());
        $this->assertEquals($hwdUnit, $package->getHwdUnit()->getValue());
        $this->assertEquals($weightUnit, $package->getWeightUnit()->getValue());
    }

    /**
     * @return array
     */
    public function testCases(): array
    {
        return [
            'Correct case' => [12.3, '1.1', 1, 2.3, 'ft', 'kg', 1.1*2.3, null],
            'Invalid height case' => [12.3, '', 1, 2.3, 'ft', 'kg', null, \InvalidArgumentException::class]
        ];
    }
}
