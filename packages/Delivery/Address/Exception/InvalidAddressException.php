<?php

namespace  Delivery\Address\Exception;

/**
 * Класс исключение при указании некорректного или несуществующего адреса
 * Class InvalidAddressException
 * @package Delivery\Address\Validation\Exception
 */
class InvalidAddressException extends \InvalidArgumentException
{
}