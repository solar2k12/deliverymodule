<?php

namespace Delivery\Address;

use Core\Validation\BaseValidation;
use Delivery\Address\Exception\InvalidAddressException;
use Delivery\Test\AddressValidationTestCases;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * Мок сервиса валидации адреса
 * Class AddressValidationServiceMock
 * @package Delivery\Address\Validation
 */
class AddressValidationServiceMock implements IAddressValidationService
{
    use BaseValidation;

    /**
     * @inheritDoc
     */
    public function validateAddress($address): void
    {
        $this->validateNonEmptyString($address);
        if (!in_array($address, AddressValidationTestCases::getAddresses(), true)) {
            throw new InvalidAddressException('Адрес не найден');
        }
    }

    /**
     * @inheritDoc
     */
    public function convertAddressToDeliveryServiceFormat(DeliveryTypeName $deliveryTypeName, $address): string
    {
        // TODO: Implement convertAddressToDeliveryServiceFormat() method.
        switch ($deliveryTypeName->getValue()) {
            default:
                return trim($address);
        }
    }
}