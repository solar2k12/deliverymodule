<?php

namespace Delivery\Address;

use Delivery\Address\Exception\InvalidAddressException;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * Интерфейс для сервиса валидации адреса
 * Interface IValidationService
 * @package Delivery\Address\Validation
 */
interface IAddressValidationService
{
    /**
     * Валидировать адрес
     * @param string $address
     * @throws InvalidAddressException
     * @return void
     */
    public function validateAddress($address): void;

    /**
     * Конвертирует адрес в формат для указанно службы
     * @param DeliveryTypeName $deliveryTypeName
     * @param string $address
     * @return string
     */
    public function convertAddressToDeliveryServiceFormat(DeliveryTypeName $deliveryTypeName, $address): string ;
}