<?php

namespace Delivery\DeliveryType;

use Core\Object\IIdObject;
use CurrencyConverter\ICurrencyConverter;
use Delivery\DeliveryType\CalculationService\IBirdDeliveryCalculationService;
use Delivery\DeliveryType\Description\BirdCalculationDescription;
use Delivery\DeliveryType\Result\DeliveryCalculationResult;
use Delivery\ValueObject\DeliveryRequest;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * Класс службы доставки "Птичка"
 * Class BirdDelivery
 * @package Delivery\DeliveryType
 */
final class BirdDelivery implements IDeliveryType, IIdObject
{
    /**
     * Маска для расчета интервала по дням
     */
    private const DAY_INTERVAL_MASK = 'P%dD';

    /**
     * Идентификатор службы доставки
     * @var string
     */
    private static $serviceId = DeliveryTypeName::BIRD_DELIVERY;

    /**
     * Сервис для расчета даты и стоимости посылки
     * @var IBirdDeliveryCalculationService
     */
    private $deliveryService;

    /**
     * Ковертор валют
     * @var ICurrencyConverter
     */
    private $currencyConverter;

    /**
     * Часовой пояс, которым оперирует служба доставки
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * BirdDelivery constructor.
     * @param IBirdDeliveryCalculationService $deliveryService
     * @param ICurrencyConverter $converter
     * @param \DateTimeZone $timeZone
     */
    public function __construct(
        IBirdDeliveryCalculationService $deliveryService,
        ICurrencyConverter $converter,
        \DateTimeZone $timeZone = null
    ) {
        $this->deliveryService = $deliveryService;
        $this->currencyConverter = $converter;
        $this->timeZone = $timeZone;
    }

    /**
     * @inheritDoc
     */
    public function getDeliveryCalculationResult(
        DeliveryRequest $request,
        $addRequest = false
    ): DeliveryCalculationResult
    {
        $result = $this->deliveryService->getDeliveryCalculationResult($request);
           $date = new \DateTime();
        if (isset($this->timeZone)) {
            $date->setTimezone($this->timeZone);
        }
        $date->add(new \DateInterval(sprintf(self::DAY_INTERVAL_MASK, $result->getDateInterval())));
        $cost = $this->currencyConverter->convertValue($result->getCost());
        $desc = $addRequest ? BirdCalculationDescription::generateDescription($result) : '';

        return new DeliveryCalculationResult($date, $cost, self::$serviceId, $desc);
    }

    /**
     * Идентификатор службы доставки
     * @return int|string
     */
    public function getId()
    {
        return self::$serviceId;
    }
}