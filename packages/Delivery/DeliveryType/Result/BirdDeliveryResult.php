<?php

namespace Delivery\DeliveryType\Result;

use Core\Validation\BaseValidation;

/**
 * Описывает данные из сервиса расчета доставки службой "Птичка"
 * Class BirdDeliveryResult
 * @package Delivery\DeliveryType\Result
 */
class BirdDeliveryResult
{
    use BaseValidation;

    /**
     * Количество дней требуемое для доставки
     * @var int
     */
    private $dateInterval;

    /**
     * Стоимость доставки
     * @var float
     */
    private $cost;

    /**
     * BirdDeliveryOrder constructor.
     * @param integer $dateInterval
     * @param float | string $cost
     * @throws \Exception
     */
    public function __construct($dateInterval, $cost = null)
    {
        $this->validatePositiveIntegerValue($dateInterval);
        $this->dateInterval = (int)$dateInterval;
        $this->validatePositiveNumericValue($cost);
        $this->cost = (float)$cost;
    }

    /**
     * Возвращает количество дней требуемое для доставки
     * @return int
     */
    public function getDateInterval(): int
    {
        return $this->dateInterval;
    }

    /**
     * Возвращает стоимость доставки
     * @return float
     */
    public function getCost(): float
    {
        return $this->cost;
    }


}