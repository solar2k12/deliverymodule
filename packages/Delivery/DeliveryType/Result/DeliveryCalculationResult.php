<?php

namespace Delivery\DeliveryType\Result;

use Core\Object\IIdObject;
use Core\Validation\BaseValidation;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * Результаты расчета доставки
 * Class DeliveryCalculationResult
 * @package Delivery\DeliveryType\Result
 */
class DeliveryCalculationResult implements IIdObject
{
    use BaseValidation;

    /**
     * Дата доставки
     * @var \DateTime
     */
    private $date;

    /**
     * Стоимость
     * @var float
     */
    private $cost;

    /**
     * @var DeliveryTypeName
     */
    private $deliveryTypeName;

    /**
     * Описание расчетов
     * @var string
     */
    private $description;

    /**
     * DeliveryCalculationResult constructor.
     * @param \DateTime $date
     * @param float $cost
     * @param string $deliveryTypeName
     * @param string $calculationDescription
     */
    public function __construct(\DateTime $date, $cost, $deliveryTypeName, $calculationDescription = null)
    {
        $this->validatePositiveNumericValue($cost);
        $this->cost = $cost;
        $this->date = $date;
        $this->deliveryTypeName = new DeliveryTypeName($deliveryTypeName);
        $this->description = $calculationDescription;
    }

    /**
     * Возвращает дату доставки
     * @return \DateTime
     */
    public function getDeliveryDate(): \DateTime
    {
        return $this->date;
    }

    /**
     * Возвращает цену доставки
     * @return float
     */
    public function getDeliveryCost(): float
    {
        return $this->cost;
    }

    /**
     * Возвращает описание расчетов
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
        return $this->deliveryTypeName->getValue();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf("
            \nID службы доставки: %s\nДата доставки: %s\nСтоимость доставки:%.2f\nОписание расчетов: %s\n",
            $this->deliveryTypeName,
            $this->date->format('Y-m-d'),
            $this->cost,
            $this->description ?? '<Отсутствует>'
        );
    }
}