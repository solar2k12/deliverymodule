<?php

namespace Delivery\DeliveryType\Result;

use Core\Object\ImmutableCollection;

/**
 * Коллекция результатов расчета доставки
 * Class DeliveryResultCollection
 * @package Delivery\DeliveryType\Result
 * @method DeliveryCalculationResult get($id)
 */
class DeliveryResultCollection extends ImmutableCollection
{
    protected static $collectionClass = DeliveryCalculationResult::class;
}