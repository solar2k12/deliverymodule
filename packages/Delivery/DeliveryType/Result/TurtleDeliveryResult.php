<?php

namespace Delivery\DeliveryType\Result;

use Core\Validation\BaseValidation;

/**
 * Описывает данные из сервиса расчета доставки службой "Черепашка"
 * Class TurtleDeliveryResult
 * @package Delivery\DeliveryType\Result
 */
class TurtleDeliveryResult
{
    use BaseValidation;

    /**
     * День доставки
     * @var \DateTime
     */
    private $date;

    /**
     * Базовая стоимость доставки
     * @var float
     */
    private $baseCost;

    /**
     * @var float
     */
    private $costCoefficient;

    /**
     * TurtleDeliveryOrder constructor.
     * @param \DateTime $date
     * @param float | string $costCoefficient
     * @param float | string $baseCost
     */
    public function __construct(\DateTime $date, $costCoefficient, $baseCost = 150.00)
    {
        $this->validatePositiveNumericValue($costCoefficient, false);
        $this->costCoefficient = (float) $costCoefficient;
        $this->validatePositiveNumericValue($baseCost, false);
        $this->baseCost = (float) $baseCost;
        $this->date = $date;
    }

    /**
     * Возвращает коэффициент стоимости
     * @return float
     */
    public function getCostCoefficient(): float
    {
        return $this->costCoefficient;
    }

    /**
     * Возвращает базовую стоимость
     * @return int
     */
    public function getBaseCost(): int
    {
        return $this->baseCost;
    }

    /**
     * Возвращает дату доставки
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }
}