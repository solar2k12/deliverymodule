<?php

namespace Delivery\DeliveryType;

use Delivery\DeliveryType\Result\DeliveryCalculationResult;
use Delivery\ValueObject\DeliveryRequest;

/**
 * Интерфейс для расчета услуг службы доставки
 * Interface IDeliveryType
 * @package Delivery\DeliveryType
 */
interface IDeliveryType
{
    /**
     * @param DeliveryRequest $request - описание заказа
     * @param boolean $addDescription - добавить описание расчетов
     * @return DeliveryCalculationResult
     */
    public function getDeliveryCalculationResult(DeliveryRequest $request, $addDescription = false);
}