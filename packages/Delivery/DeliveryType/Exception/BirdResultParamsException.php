<?php

namespace Delivery\DeliveryType\Exception;

/**
 * Исключение при некорректном ответе от службы доставки "Птичка"
 * Class BirdResponseException
 * @package Delivery\DeliveryType\Exception
 */
class BirdResultParamsException extends \RuntimeException
{
}