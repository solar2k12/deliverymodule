<?php

namespace Delivery\DeliveryType\Exception;

/**
 * Исключение при запросе в сервис расчета доставки для службы "Черепашка"
 * Class TurtleServiceException
 * @package Delivery\DeliveryType\Exception
 */
class TurtleServiceException extends \RuntimeException
{
}