<?php

namespace Delivery\DeliveryType\Exception;

/**
 * Исключение при некорректном ответе от службы доставки "Черепашка"
 * Class TurtleResultParamsException
 * @package Delivery\DeliveryType\Exception
 */
class TurtleResultParamsException extends \RuntimeException
{
}