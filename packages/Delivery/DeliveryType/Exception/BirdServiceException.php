<?php

namespace Delivery\DeliveryType\Exception;

/**
 * Исключение при запросе в сервис расчета доставки для службы "Птичка"
 * Class BirdServiceException
 * @package Delivery\DeliveryType\Exception
 */
class BirdServiceException extends \RuntimeException
{
}