<?php

namespace Delivery\DeliveryType;

use Core\Object\IIdObject;
use CurrencyConverter\ICurrencyConverter;
use Delivery\DeliveryType\CalculationService\ITurtleDeliveryCalculationService;
use Delivery\DeliveryType\Description\TurtleCalculationDescription;
use Delivery\DeliveryType\Exception\TurtleServiceException;
use Delivery\DeliveryType\Result\DeliveryCalculationResult;
use Delivery\ValueObject\DeliveryRequest;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * Класс службы доставки "Черепашка"
 * Class TurtleDelivery
 * @package Delivery\DeliveryType
 */
final class TurtleDelivery implements IDeliveryType, IIdObject
{
    /**
     * Идентификатор службы доставки
     * @var string
     */
    private static $serviceId = DeliveryTypeName::TURTLE_DELIVERY;

    /**
     * Сервис для расчета даты и стоимости посылки
     * @var ITurtleDeliveryCalculationService
     */
    private $deliveryService;

    /**
     * Ковертор валют
     * @var ICurrencyConverter
     */
    private $currencyConverter;

    /**
     * Часовой пояс, которым оперирует служба доставки
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * TurtleDelivery constructor.
     * @param ITurtleDeliveryCalculationService $deliveryService
     * @param ICurrencyConverter $converter
     * @param \DateTimeZone $timeZone
     */
    public function __construct(
        ITurtleDeliveryCalculationService $deliveryService,
        ICurrencyConverter $converter,
        \DateTimeZone $timeZone = null
    ) {
        $this->deliveryService = $deliveryService;
        $this->currencyConverter = $converter;
        $this->timeZone = $timeZone;
    }

    /**
     * @inheritDoc
     */
    public function getDeliveryCalculationResult(
        DeliveryRequest $request,
        $addRequest = false
    ): DeliveryCalculationResult
    {
        $result = $this->deliveryService->getDeliveryCalculationResult($request);
        if (!$result) {
            throw new TurtleServiceException('Сервис не смог обработать запрос');
        }
        $date = $result->getDate();
        if ($this->timeZone) {
            $date->setTimezone($this->timeZone);
        }

        $cost = $this->currencyConverter->convertValue($result->getBaseCost() * $result->getCostCoefficient());
        $desc = $addRequest ? TurtleCalculationDescription::generateDescription($result) : '';

        return new DeliveryCalculationResult($date, $cost, self::$serviceId, $desc);
    }

    /**
     * @inheritDoc
     */
    public function getId()
    {
       return self::$serviceId;
    }
}