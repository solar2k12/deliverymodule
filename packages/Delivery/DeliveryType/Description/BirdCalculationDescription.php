<?php

namespace Delivery\DeliveryType\Description;

use Delivery\DeliveryType\Result\BirdDeliveryResult;

/**
 * Описание расчетов цены и даты доставки для службы "Черепашка"
 * Class BirdCalculationDescription
 * @package Delivery\DeliveryType\Description
 */
class BirdCalculationDescription
{
    public const DESCRIPTION_TEMPLATE = 'Общая стоимость: %.2f. Количество дней: %d';

    /**
     * Генерирует описание
     * @param BirdDeliveryResult $result
     * @return string
     */
    public static function generateDescription(BirdDeliveryResult $result): string
    {
        return sprintf(self::DESCRIPTION_TEMPLATE, $result->getCost(), $result->getDateInterval());
    }
}