<?php

namespace Delivery\DeliveryType\Description;

use Delivery\DeliveryType\Result\TurtleDeliveryResult;

/**
 * Описание расчетов цены и даты доставки для службы "Черепашка"
 * Class TurtleCalculationDescription
 * @package Delivery\DeliveryType\Description
 */
class TurtleCalculationDescription
{
    //Шаблон для описания расчетов
    public const DESCRIPTION_TEMPLATE =
        'Ценообразование состоит из базовой цены %.2f умноженной на коэффициент %.2f. Дата доставки: %s';

    /**
     * Генерирует описание
     * @param TurtleDeliveryResult $result
     * @return string
     */
    public static function generateDescription(TurtleDeliveryResult $result): string
    {
        return sprintf(
            self::DESCRIPTION_TEMPLATE,
            $result->getBaseCost(),
            $result->getCostCoefficient(),
            $result->getDate()->format('Y-m-d')
        );
    }
}