<?php

namespace Delivery\DeliveryType\CalculationService;

use Delivery\DeliveryType\Result\TurtleDeliveryResult;
use Delivery\ValueObject\DeliveryRequest;

/**
 * Интерфейс для описания сервиса из которого возвращается стоимость и дата доставки для службы "Черепашка"
 * Interface ITurtleDeliveryCalculationService
 * @package Delivery\DeliveryType\DeliveryService
 */
interface ITurtleDeliveryCalculationService
{
    /**
     * @param DeliveryRequest $request
     * @return TurtleDeliveryResult
     */
    public function getDeliveryCalculationResult(DeliveryRequest $request): TurtleDeliveryResult;
}