<?php

namespace Delivery\DeliveryType\CalculationService;

use Core\Validation\DateTimeValidation;
use Delivery\DeliveryType\Exception\TurtleResultParamsException;
use Delivery\DeliveryType\Exception\TurtleServiceException;
use Delivery\DeliveryType\Result\TurtleDeliveryResult;
use Delivery\Test\CalculationTestCases;
use Delivery\ValueObject\DeliveryRequest;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * "Заглушка" сервиса для расчета доставки службой "Черепашка"
 * Полноценный сервис должен реализовывать интерфейс ITurtleDeliveryCalculationService
 * Class TurtleDeliveryCalculationServiceMock
 * @package Delivery\DeliveryType\DeliveryService
 */
final class TurtleDeliveryCalculationServiceMock implements ITurtleDeliveryCalculationService
{
    use DateTimeValidation;

    /**
     * Формат даты в котором передается дата отправки
     * @var string
     */
    private $dateFormat;

    /**
     * Базовая цена по умолчанию
     * @var float
     */
    private $defaultBaseCost;

    /**
     * TurtleDeliveryCalculationServiceMock constructor.
     * @param string $dateFormat
     * @param float $defaultBaseCost
     */
    public function __construct($dateFormat, $defaultBaseCost)
    {
        $this->dateFormat = $dateFormat;
        $this->defaultBaseCost = $defaultBaseCost;
    }

    /**
     * @param DeliveryRequest $request
     * @return TurtleDeliveryResult
     * @throws \Exception
     */
    public function getDeliveryCalculationResult(DeliveryRequest $request): TurtleDeliveryResult
    {
        $result = CalculationTestCases::getResponse(DeliveryTypeName::getTurtleType(), $request);
        $this->validateResult($result);

        return new TurtleDeliveryResult(
            new \DateTime($result['date']),
            $result['costCoefficient'],
            $result['baseCost'] ?? $this->defaultBaseCost
        );
    }

    /**
     * @param array $result
     */
    private function validateResult(array $result):void
    {
        if (!$result) {
            throw new TurtleServiceException('Сервис не смог обработать запрос');
        }

        if (!isset($result['date'], $result['costCoefficient'])) {
            throw new TurtleResultParamsException('Неожиданный формат ответа');
        }

        $this->validateDateTimeFromString($result['date'], $this->dateFormat);
    }
}