<?php

namespace Delivery\DeliveryType\CalculationService;

use Delivery\DeliveryType\Exception\BirdResultParamsException;
use Delivery\DeliveryType\Exception\BirdServiceException;
use Delivery\DeliveryType\Exception\TurtleResultParamsException;
use Delivery\DeliveryType\Result\BirdDeliveryResult;
use Delivery\ValueObject\DeliveryRequest;
use Delivery\Test\CalculationTestCases;
use Delivery\ValueObject\DeliveryTypeName;

/**
 * "Заглушка" сервиса для расчета доставки службой "Птичка"
 * Полноценный сервис должен реализовывать интерфейс IBirdDeliveryCalculationService
 * Class BirdDeliveryCalculationServiceMock
 * @package Delivery\DeliveryType\DeliveryService
 */
final class BirdDeliveryCalculationServiceMock implements IBirdDeliveryCalculationService
{
    /**
     * @param DeliveryRequest $request
     * @return BirdDeliveryResult
     * @throws \Exception
     */
    public function getDeliveryCalculationResult(DeliveryRequest $request): BirdDeliveryResult
    {

        $result = CalculationTestCases::getResponse(DeliveryTypeName::getBirdType(), $request);
        $this->validateResult($result);

        return new BirdDeliveryResult($result['dateInterval'], $result['cost']);
    }

    /**
     * @param $result
     */
    private function validateResult($result): void
    {
        if (!$result) {
            throw new BirdServiceException('Сервис не смог обработать запрос');
        }
        if (!isset($result['dateInterval'], $result['cost'])) {
            throw new BirdResultParamsException('Неверный формат ответа');
        }
    }
}