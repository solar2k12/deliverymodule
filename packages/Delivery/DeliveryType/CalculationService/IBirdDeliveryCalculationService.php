<?php

namespace Delivery\DeliveryType\CalculationService;

use Delivery\DeliveryType\Result\BirdDeliveryResult;
use Delivery\ValueObject\DeliveryRequest;

/**
 * Интерфейс для описания сервиса из которого возвращается стоимость и дата доставки для службы "Птичка"
 * Interface IBirdDeliveryCalculationService
 * @package Delivery\DeliveryType\DeliveryService
 */
interface IBirdDeliveryCalculationService
{
    /**
     * @param DeliveryRequest $request
     * @return BirdDeliveryResult
     */
    public function getDeliveryCalculationResult(DeliveryRequest $request): BirdDeliveryResult;
}