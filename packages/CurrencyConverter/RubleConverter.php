<?php

namespace CurrencyConverter;

use Core\Validation\BaseValidation;

/**
 * Округление рубля
 * Class RubbleConvert
 * @package CurrencyConvert
 */
final class RubleConverter implements ICurrencyConverter
{
    use BaseValidation;

    /**
     * Округление до сотых
     */
    public const DEFAULT_RADIUS = 2;

    /**
     * Возвращает округленное значение до сотых
     * @param float $value
     * @return float
     */
    public function convertValue($value): float
    {
        $this->validatePositiveFloatValue($value);
        return round($value, self::DEFAULT_RADIUS);
    }

}