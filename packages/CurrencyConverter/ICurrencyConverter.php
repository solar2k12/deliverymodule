<?php

namespace CurrencyConverter;

/**
 * Интерфейс для ковертации и округления различных валют
 * Interface ICurrencyConverter
 * @package CurrencyConvert
 */
interface ICurrencyConverter
{
    /**
     * @param float $value
     * @return float
     */
    public function convertValue($value): float;
}