<?php
define('CLASS_DIR', __DIR__.DIRECTORY_SEPARATOR.'packages'.DIRECTORY_SEPARATOR);
define('COMPOSER_AUTOLOAD', __DIR__.DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php');
spl_autoload_register(static function($className) {
    include_once CLASS_DIR . str_replace('\\', DIRECTORY_SEPARATOR, $className) . '.php';
});

if (file_exists(COMPOSER_AUTOLOAD)) {
    include_once COMPOSER_AUTOLOAD;
}

@set_exception_handler(static function (Throwable $ex) {
    //TODO: переделать на лог в файл
    echo sprintf("Что-то пошло не так, а конкретно вот это: \n\t\"%s\"\n", $ex->getMessage());
    echo "Произошло это вот тут:\n";
    foreach ($ex->getTrace() as $exceptionDesc) {
      echo sprintf("\t%s:%d\n", $exceptionDesc['file'], $exceptionDesc['line']);
    }
});