<?php
include_once 'init.php';

use CurrencyConverter\RubleConverter;
use Delivery\DeliveryService;
use Delivery\Address\AddressValidationServiceMock;
use Delivery\DeliveryType\BirdDelivery;
use Delivery\DeliveryType\CalculationService\BirdDeliveryCalculationServiceMock;
use Delivery\DeliveryType\CalculationService\TurtleDeliveryCalculationServiceMock;
use Delivery\DeliveryType\TurtleDelivery;
use Delivery\DeliveryTypeCollection;
use Delivery\Test\CalculationTestCases;
use Delivery\ValueObject\DeliveryTypeName;

//TODO: перенести в конфигурационный файл configuration/<env>/delivery.php
define('DEFAULT_TURTLE_COST', 150);

/**
 * Вспомогательная функция для получения расчета по заказу
 * @param DeliveryService $service
 * @param array $deliveryTypes
 */
function testDeliveryServiceCalculation(DeliveryService $service, array $deliveryTypes = [])
{
    $types = $deliveryTypes;
    foreach (CalculationTestCases::getRequests() as $request) {
        if (empty($deliveryTypes)) {
            $types = DeliveryTypeName::getAvailableValues();
            $responseCollection = $service->getDeliveryConditions($request, [], true);
        }
        foreach ($types as $typeValue) {
            if (!empty($deliveryTypes)) {
                $responseCollection = $service->getDeliveryConditions($request, [$typeValue], true);
            }
            if ($responseCollection->isEmpty()) {
                throw new \LogicException('Пустой ответ!');
            }
            $response = $responseCollection->get($typeValue);
            echo $request;
            echo $response;

        }
    }
}

//По хорошему цепочку инициализаций ниже нужно бы перенсти в Pimple конейнер
//Класс для конвертации и округления
$currencyConverter = new RubleConverter();
//"Заглушка" получения данных для службы "Птичка"
$birdCalculationService = new BirdDeliveryCalculationServiceMock();
//"Заглушка" получения данных для службы "Черепашка"
$turtleCalculationService = new TurtleDeliveryCalculationServiceMock(DATE_ATOM, DEFAULT_TURTLE_COST);
// Сервис для обработки данных службы "Птичка"
$birdDelivery = new BirdDelivery($birdCalculationService, $currencyConverter, new DateTimeZone('Africa/Abidjan'));
// Сервис для обработки данных службы "Черепашка"
$turtleDelivery = new TurtleDelivery(
    $turtleCalculationService,
    $currencyConverter,
    new DateTimeZone('Europe/Busingen')
);
// Коллекция из всех доступных служб доставки
$deliveryTypes = new DeliveryTypeCollection([$birdDelivery, $turtleDelivery]);
// "Заглушка" сервиса проверки и конвертации адреса
$addressValidationService = new AddressValidationServiceMock();
// Основной сервис
$deliveryService = new DeliveryService($addressValidationService, $deliveryTypes);

// $deliveryTypes - пусто, а следовательно получаем расчеты из всех доступных служб
echo "\nРасчет по всем службам...\n";
//Запуск генерации данных для заглушек.
CalculationTestCases::generateCases();
testDeliveryServiceCalculation($deliveryService);

echo "\nРасчет для службы \"Птичка\"...\n";
CalculationTestCases::generateCases();
testDeliveryServiceCalculation($deliveryService, [DeliveryTypeName::BIRD_DELIVERY]);

echo "\nРасчет для службы \"Черепашка\"...\n";
CalculationTestCases::generateCases();
testDeliveryServiceCalculation($deliveryService, [DeliveryTypeName::TURTLE_DELIVERY]);